resource "aws_route_table_association" "pub-sub-rt" {
  subnet_id      = aws_subnet.pub_sub.id
  route_table_id = aws_route_table.pub_rt.id
}

resource "aws_route_table_association" "priv-sub-rt" {
  subnet_id      = aws_subnet.priv_sub.id
  route_table_id = aws_route_table.priv_rt.id
}