output "vpc_id" {
  value = aws_vpc.rr.id
}
# output pub subnet id

output "pub_sub_id" {
  value = aws_subnet.pub_sub.id
}