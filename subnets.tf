resource "aws_subnet" "pub_sub" {
  vpc_id     = aws_vpc.rr.id
  cidr_block = var.pub_subnet_cidr
  availability_zone = var.pub_availability_zone

  }

 resource "aws_subnet" "priv_sub" {
  vpc_id     = aws_vpc.rr.id
  cidr_block = var.priv_subnet_cidr
  availability_zone = var.priv_availability_zone

  }