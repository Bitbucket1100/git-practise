resource "aws_route_table" "pub_rt" {
    vpc_id = aws_vpc.rr.id
  }
# private route table added
  resource "aws_route_table" "priv_rt" {
    vpc_id = aws_vpc.rr.id
  }