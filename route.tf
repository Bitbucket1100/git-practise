resource "aws_route" "pub_route" {
  route_table_id            = aws_route_table.pub_rt.id
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.ig1.id
  depends_on                = [aws_route_table.pub_rt]
}