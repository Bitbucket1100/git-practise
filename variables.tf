variable "region" {
    type = string
}
variable "vpc_cidr" {
    type = string
}
variable "pub_subnet_cidr" {
    type = string
}
variable "priv_subnet_cidr" {
    type = string
}
variable "priv_availability_zone" {
    type = string
}
variable "pub_availability_zone" {
    type = string
}