# create vpc in aws
provider "aws" {
  region = var.region
}

resource "aws_vpc" "rr" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"
  }

